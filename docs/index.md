# Welcome to Kaiser Software's Chassis general documentation
Welcome to the general documentation of Kaiser Software's Chassis framework.

# General information
Kaiser Software's Chassis framework is based on open-source frameworks.  
## Backend API server
For the backend we use the popular framework [Laravel](https://laravel.com).

## Frontend client
For the frontend we use the highly dynamic framework [Angular2](https://angular.io).

## Mobile application
For the mobile applications we use [Ionic](https://ionicframework.com)

# Requirements
The following software is required to setup the Chassis framework:
1. [Git](https://git-scm.com)  
2. Webserver (Apache/Nginx, MySQL, PHP) like [MAMP](https://www.mamp.info), [XAMPP](https://www.apachefriends.org), etc.  
3. [Composer](https://getcomposer.org)  
4. [Node.js](https://nodejs.org)  
5. [Yarn](https://yarnpkg.com)  
6. [Android Studio](https://developer.android.com/studio)  
7. [XCode](https://developer.apple.com/xcode)  
8. An editor like [PHPStorm](https://www.jetbrains.com/phpstorm), [Atom](https://atom.io), [Sublime Text](https://www.sublimetext.com/3), etc.